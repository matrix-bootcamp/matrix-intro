---
room_alias: #about-matrix:matrix.org
---

# matrix.org

> An open network for secure, decentralised communication.

Chat with friends, family, communities and co-workers (similarely to
other apps, Telegram, Signal, Slack, Messenger, Whatsapp, Teams etc.),
organize conversations in rooms and spaces, publish content to the
web, integrate with other ecosystems.

- https://matrix.org/
- https://matrix.org/docs/chat_basics/matrix-for-im/
- https://matrix.org/about describes its missions in a manisfesto

# Advantages of matrix

A summary of why we should try/use matrix.

- works on all devices and operating systems (without requiring a
phone number)
- open standard for encrypted communications, and free software
- can DM contacts and chat in rooms, organize in spaces
- users and rooms have addresses (like emails)
- decentralized (like emails), can host to get a personal/organization
domain address for free
- interoperable, can bridge conversations with other chat ecosystems
(Signal, IRC, Telegram, Whatsapp, etc.)
