---
room_alias: #how-matrix-works:matrix.org
---
# How matrix works
Overview of "what it takes" to use and communicate information (chat,
content, data etc.) over the matrix networks.

## Matrix addresses for usernames, groups
Matrix users and rooms can be referenced by their matrix address.

### For matrix `users` (DM)
A **matrix address** has this aspect `@username:server.tld`

For example, to show the similarity between an email and a matrix address:
```
# email
user@example.com

# matrix
@user:example.com
```

> On other messenger applications (whatsapp, telegram, instagram,
> Slack etc.), users find each other by their phone number or
> usernames, email etc.. A matrix address is similar to a username
> (but with th "matrix server part" at the end), and as unique as a
> phone number.

### For matrix `rooms` (groups)
On matrix, a group is a `room`. A room has an address `#room_name:server.tld` (a bit like a matrix "user address").

It starts with the symbol `#`, when for a user it starts with the
symbol `@` so it is possible to know if we are connecting with a user
or a room.

## matrix `server`(s), `homeserver`(s) and `client`(s)
Examplain a few terms to understand better how matrix works.

### matrix homeservers and servers
- a `homeserver` is the domain where a user has created their user (on
  a matrix server), or on which a room exists
- a `server` (opposed to `client`), is the code that runs the `homeserver`

> Sometimes `server` is used to refer to `homeserver`

### matrix clients
A software that we use on our device, and that we can use to connect
to a matrix homeserver, use our user account, chat, join room.

For examples:
- [element.io](https://element.io)
- [fluffy chat](https://fluffychat.im)
- [more matrix clients](https://matrix.org/ecosystem/clients)

> These client apps can be used from web-browsers, mobile and desktop
> devices. There are many more clients to be tried, and which are
> improved constantly, as part of the community ecosystem.

### `element` web client
On https://app.element.io we can access the `element` web client.

> We chose element, because it works on every device and platform.

## Specification
Because matrix.org is "an open protocol", all technical and
philosophical instructions are written in "plain english"; See the
matrix specification https://spec.matrix.org

## Bridges & integrations
Bridges allow us to connect Matrix to a third-party platform, and
interact seamlessly (matrix <-> slack, instagram, microsoft-teams,
google-teams etc. 100+).

## Bots
It is possible to have bots users on matrix, that do some actions when
new messages are posted in a chat room (and many other things), or
post to chat rooms when things happen elswehere on the internet.
