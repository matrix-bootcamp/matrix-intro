---
room_alias: #create-matrix-user:matrix.org
---
# Register new matrix user account
To use matrix fully as a registered user (not a guest user), we need
to create a user account.

> Maybe our organization's adminstrator already created a user account
> for us, otherwise we can find a public homeserver on which we can
> register a new one.

# Chose a client
To use matrix on our machines (computers, phones etc.) we need to
install a "client" application, similar to any other chat application.

A good client to start with is [element](https://element.io/), which
works in web browsers, and also has applications for all platforms
(desktop, mobiles etc.)

# Chose a homeserver
Because matrix is decentralized like emails are, we can create a
matrix user account on different homeservers (like we can chose
different servers for our email account `@example.org`).

> Some homeservers are open to new user registration, some are private
> to groups and organisations (like company email addresses are).

A good first homeserver is `matrix.org`, offered freely by the matrix
foundation.

# Create a matrix user
We can now create our matrix user account, on the `matrix.org`
homeserver using the `element` web client (accessed with the Firefox
browser at the URL address `https://app.element.io`).

- follow the steps to "Create an account" on the element client
- our matrix user name/ID, `@username:homeserver.tld` cannot be
  changed after registration (but we could register a new one)
- use a strong password, and a password manager to store our user
  password and the encryption keys matrix asks us to backup
- maybe confirm the account creation in our email inbox

> Once logged in, Element offers us to select a "backup strategy" for
> our user account encryption keys. We can either "Generate a Security
> Key" or "Enter a Security Phrase". Both methods requires us to store
> this data somewhere safe, like a "password manager". We can do the
> backup part later, but it is important to do at some point.

# Next steps
We can explore (and will discover) how to:
- explore and join `rooms` and `spaces`, for example try `#matrix-bootcamp:matrix.org` (a
  room_alias starts with `#`)
- send direct messages to a user, using their user_id, for example try
  `@matrix-bootcamp:matrix.org`
