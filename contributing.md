# Development and documentation
For information on how to contribute to these tutoriels.
- video recordings are made using [Open Broadcast System (OBS)](https://obsproject.com)
- in the video the text is written on linux using
  [emacs](https://www.gnu.org/software/emacs), one can also use any
  text editor and environment
- To open `.tldr` files use the web-app https://www.tldraw.com
- Use the gitlab interface, or git to make changes to this project

## Convert `.mkv` videos to `.webm`
It requires the `ffmpg` software.
```bash
# convert
for i in *.mkv; do ffmpeg -i "$i" -c:v libvpx -an "${i%.*}.webm"; done

# or for better video quality and no audio
for i in *.mkv; do ffmpeg -i $i -c:v libvpx-vp9 -b:v 2000k -an "${i%.*}.webm"; done
```

Alternatively to preserve quality of video and reduce file size:
```bash
# with audio
ffmpeg -i input.mkv -c:v libvpx-vp9 -b:v 2000k -c:a libvorbis -b:a 128k output.webm

# without audio
ffmpeg -i input.mkv -c:v libvpx-vp9 -b:v 2000k -an output_no_audio.webm
```
