---
room_alias: #readme-matrix:matrix.org
---
# matrix.org introduction
Some content to introduce and present what is the
[matrix.org](https://matrix.org/) ecosystem.

We will learn how to get started with a user account, chat in rooms,
chat and DM with friends and contacts, use encryption, use rooms to
store private or public content, organize chat and content for our
projects and organization, re-use our chat content and publish it on
web-pages…

## How to use this guide/tutorial/introduction?
To learn more about matrix and get started as a new user:
- explore the ecosystem starting from https://matrix.org
- [read the content of the files in the tutorials's
  folder](https://gitlab.com/matrix-bootcamp/matrix-intro), and watch
  the videos with the same name as each lesson
- ask questions in the community chat, and search the web for articles & videos
- try matrix for ourselves, using the [element](https://element.io)
  matrix "client" (works on all operating systems and in all
  web-browsers)
- ask question in the community chat
  [#matrix-intro:matrix.org](https://matrix.to/#/#matrix-intro:matrix.org)
  or in a git issue
- visit this tutorial's dedicated matrix space
  [#matrix-bootcamp:matrix.org](https://matrix.to/#/#matrix-bootcamp:matrix.org)
- [explore the same matrix space with libli](
  https://libli.org/#matrix-bootcamp:matrix.org) (because the space
  and some of its rooms are public)
